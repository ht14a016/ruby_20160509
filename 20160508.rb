# encoding: utf-8
require 'time'
require 'benchmark'

def convtime(_str)
    m = _str.match(/^(.*)\/(.*)\/(.*):(.*):(.*):(.*) (.*)$/)
    Time.parse("#{m[3]}-#{m[2]}-#{m[1]} #{m[4]}:#{m[5]}:#{m[6]}").to_i
end

lasttime = 0
mb = [{time: 0,size: 0}]
hb = [{time: 0,size: 0}]
result = Benchmark.realtime do
  File.open('./apache.log.txt', 'r:utf-8') do |f|
      while line = f.gets
          m = line.match(/^(.*) (.*) (.*) \[(.*)\] "(.*)" (.*) (.*) "(.*)" "(.*)"$/)
          lasttime = convtime(m[4])
          mb.push(time: lasttime, size: m[7].to_i)
          mb.shift while mb[0][:time] <= lasttime - 60
          hb.push(time: lasttime, size: m[7].to_i)
          hb.shift while hb[0][:time] <= lasttime - 60
          puts '計算中……'
      end
    end
end

m1sam = mb.inject(0) { |sum, n| sum + n[:size] }
h1sam = hb.inject(0) { |sum, n| sum + n[:size] }

puts "直近1分 #{m1sam}/byte"
puts "直近1時間 #{h1sam}/byte"
puts "実行時間 #{result}/s"
puts "要素数 #{mb.length}"
